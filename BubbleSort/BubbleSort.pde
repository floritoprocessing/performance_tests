float[] array;
int startTime;
int endTime;
int mode;

int sortType = 0;

void setup() {
  size(1000,400,P3D);
  mode = 0;
}

void mousePressed() {
  mode++;
}



void bubbleSort(float[] array) {
  // START BUBBLE SORT:
  // -----------------

  float v1=0;
  float v2=0;
  int nBegin = 0;
  int nEnd=array.length-1;

  int lastSwap=0;
  int firstSwap=0;
  boolean swapped=true;
  boolean notYetSwapped=true;
  while (swapped) {
    swapped=false;
    lastSwap=0;
    notYetSwapped=true;
    for (int i=nBegin;i<nEnd;i++) {
      v1=array[i];
      v2=array[i+1];
      if (v1>v2) {
        array[i]=v2;
        array[i+1]=v1;
        swapped=true;
        lastSwap=i;
        if (!swapped) {
          notYetSwapped=false;
          firstSwap=i;
        }
      }
    }
    nBegin = firstSwap;
    nEnd = lastSwap;//--;
  }

  // BUBBLE SORT END
  // ---------------
}





void groupedBubbleSort(float[] array,int splitSize) {
  int nrOfGroups = array.length/splitSize;
  float[][] splitArray = new float[nrOfGroups][splitSize];
  for (int group=0;group<nrOfGroups;group++) {
    for (int i=0;i<splitSize;i++) {
      splitArray[group][i] = array[group*splitSize+i];
    }
    bubbleSort(splitArray[group]);
  }

  float[] groupArray = new float[nrOfGroups];
  for (int i=0;i<splitSize;i++) {
    for (int group=0;group<nrOfGroups;group++) {
      groupArray[group] = splitArray[group][i];
      bubbleSort(groupArray);
      array[i*nrOfGroups+group] = groupArray[group];
    }
  }
  bubbleSort(array);
}






void draw() {
  if (mode==0) {
    println();
    println("START!");
    print("creating array...");
    startTime=millis();
    array=new float[20000];
    for (int i=0;i<array.length;i++) array[i]=random(height);
    println(millis()-startTime+" ms");
    background(0);
    stroke(255,255,255,16);
    for (int i=0;i<array.length;i++) line(width*i/(float)array.length,height,width*i/(float)array.length,height-array[i]);
    mode=1;
  } 
  else if (mode==1) {
    if (sortType==0) {
      print("click to start sorting array with classic bubblesort...");
    } 
    else if (sortType==1) {
      print("click to start sorting array with split-grouped bubblesort...");
    }
    mode=2;
  } 
  else if (mode==2) {

  } 
  else if (mode==3) {
    startTime=millis();

    if (sortType==0) {
      
      bubbleSort(array);
      sortType++;
      
    } 
    else if (sortType==1) {

      int splitSize=1000;
      print("splitSize: "+splitSize+".. ");
      groupedBubbleSort(array,splitSize);


      sortType=0;
    }

    endTime=millis()-startTime;
    println(endTime+" ms");
    mode=4;
  } 
  else if (mode==4) {
    background(0);
    stroke(255,255,255,16);
    for (int i=0;i<array.length;i++) line(width*i/(float)array.length,height,width*i/(float)array.length,height-array[i]);
    stroke(255,0,0,128);
    line(0,height,width,0);
    mode++;

    println("sorted "+array.length+" elements, that is "+array.length/(float)endTime+" elements per millisecond");
  } 
  else if (mode==5) {

  } 
  else if (mode==6) {
    mode=0;
  }
}
