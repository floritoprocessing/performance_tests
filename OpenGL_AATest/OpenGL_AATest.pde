/**
 * OpenGL_AATest.pde
 * For comparing the quality of OpenGL anti-aliasing among video cards.
 * Dave Bollinger, Apr 2007
 */

import processing.opengl.*;

static final int MODE_STROKE = 0;
static final int MODE_FILL = 1;
static final int MODE_FILL_STROKE_DARK = 2;
static final int MODE_FILL_STROKE_FG = 3;

void setup() {
  size(320, 480, OPENGL);
}

void draw() {
  test(0,255);
  PImage imgB = get();
  test(255,0);
  PImage imgW = get();
  saveit(imgB, imgW, "results.tif");
  exit();
}


void test(color bg, color fg) {
  background(bg);
  lines(fg);
  circles(bg, fg);
  triangles(180, MODE_STROKE, fg);
  lights();
  triangles(220, MODE_FILL, fg);
  triangles(260, MODE_FILL_STROKE_DARK, fg);
  triangles(300, MODE_FILL_STROKE_FG, fg);
}

void lines(color fg) {
  stroke(fg);
  strokeWeight(1f);
  for (int i=-12; i<=12; i++)
    line(0,240,160,240+i*20);
}

void circles(color bg, color fg) {
  strokeWeight(1f);
  for (int r=90; r>=10; r-=10) {
    fill(fg);
    stroke(bg);
    ellipse(52, 52, r, r);
    color temp=fg; 
    fg=bg; 
    bg=temp;
  }
}

void triangles(float x, int mode, color fg) {
  strokeWeight(1f);
  colorMode(HSB);
  for (int i=0; i<10; i++) {
    switch(mode) {
    case MODE_STROKE :
      noFill();
      stroke(fg);
      break;
    case MODE_FILL :
      fill(color((float)(i)*25, 255, 255));
      noStroke();
      break;
    case MODE_FILL_STROKE_DARK :
      fill(color((float)(i)*25, 255, 255));
      stroke(color((float)(i)*25, 128, 128));
      break;
    case MODE_FILL_STROKE_FG :
      fill(color((float)(i)*25, 255, 255));
      stroke(fg);
      break;
    }
    triangle(x-18, i*48+24,
    x+18-i, i*48+24-4-i*2,
    x+18-i*2, i*48+24+4+i*2);
  }
}

// assemble individual tests into composite result image
void saveit(PImage imgB, PImage imgW, String filename) {
  PImage compo = createImage(640, 480, ARGB);
  // use set() not copy() so not even a chance of filtering during composition
  compo.set(0, 0, imgB);
  compo.set(320, 0, imgW);
  // set smooth to false to ensure no filtering occurs when scaling the line magnifier view
  //compo.smooth = false;
  compo.copy(imgB, 20,230,20,20, 10,390,80,80);
  compo.copy(imgW, 20,230,20,20, 330,390,80,80);
  // save combined image
  compo.save(savePath(filename));
  compo = null;
} 
