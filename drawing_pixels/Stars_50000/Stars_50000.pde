float[] x;
float[] y;
float[] z;
float sf=0.1;

void setup() {
  size(800,600,P3D);
  loadArray();
}

void draw() {
  background(0); stroke(255);
  
  
  translate(width/2.0,height/2.0);
  rotateX(PI*(mouseY-height/2.0)/(float)height);
  rotateY(-PI*(mouseX-width/2.0)/(float)width);
  for (int i=0;i<50000;i++) {
    point(x[i]*sf,y[i]*sf,z[i]*sf);
  }
}

void keyPressed() {
  if (key=='q') {sf*=1.01;}
  if (key=='a') {sf/=1.01;}
  if (key=='Q') {sf*=1.1;}
  if (key=='A') {sf/=1.1;}
}






void loadArray() {
x=new float[50000]; y=new float[50000]; z=new float[50000];
  for (int i=0;i<50000;i++) {
    x[i]=random(-500,500); y[i]=random(-500,500); z[i]=random(-500,500); 
  }
}
